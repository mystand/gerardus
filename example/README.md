# Example

### Prepare Gerardus

```shell
git clone git@bitbucket.org:mystand/gerardus.git
cd gerardus
npm install
npm run build
```

### Start simple server

```shell
cd example
python -m SimpleHTTPServer 8000
```

### Run Gerardus

```shell
../bin/gerardus --verbose russian-subjects.mbtiles
```

### Open browser

```shell
open http://localhost:8000 
```
