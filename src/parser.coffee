ArgumentParser = require('argparse').ArgumentParser

parser = new ArgumentParser
  version: '0.0.20',
  addHelp: true,
  description: 'simplest mbtiles server'

parser.addArgument [ '-c', '--config' ],
  help: 'Load the configuration found in filename.'

parser.addArgument [ '-d', '--default-tile' ],
  help: 'Image that will get the client if the requested tile is not found'

parser.addArgument [ '--force-tms' ],
  help: 'Force response as TMS (default false)'
  action: 'storeTrue'
  defaultValue: false

parser.addArgument [ '--host' ],
  help: 'Server hostname'
  defaultValue: 'localhost'

parser.addArgument [ '-l', '--log' ],
  help: 'Log file (default gerardus.log)'
  defaultValue: 'gerardus.log'

parser.addArgument [ '-p', '--port' ],
  help: 'Port (default 8888)'
  defaultValue: 8888

parser.addArgument [ '--pid' ],
  help: 'PID file'

parser.addArgument [ '-s', '--socket' ],
  help: 'Unix socket to listen'

parser.addArgument ['tiles'],
  nargs: '*'
  help: 'list of .mbtiles'

parser.addArgument [ '--verbose' ],
  help: 'Verbose output (default false)'
  action: 'storeTrue'
  defaultValue: false

module.exports = parser