_         = require 'underscore'
fs        = require 'fs'
npid      = require 'npid'
http      = require 'http'
yaml      = require 'js-yaml'
parser    = require './parser'
winston   = require 'winston'
express   = require 'express'
sqlite3   = require 'sqlite3'
strftime  = require 'strftime'

# Prepare configuration

args = parser.parseArgs()

config = {}
if args.config
  try
    config = yaml.safeLoad fs.readFileSync args.config, 'utf8'

  catch error
    console.log 'Can not parse configuration file, terminating.
                 Message: ' + error.message
    
    process.exit -1

for arg in ['default_tile', 'force_tms', 'host', 'log', 'pid', 'port', 'socket', 'tiles', 'verbose']
  config[arg] = args[arg] if args[arg]

logger = new winston.Logger
  transports: [
    new winston.transports.Console
      colorize: 'true'
      level: 'error' unless config.verbose
    new winston.transports.File
      json: false
      filename: config.log
      maxsize: 1024 * 1024 * 10, # 10MB
      timestamp: -> strftime '%F %T', new Date Date.now()]

unless config.tiles?.length
  logger.log 'error', 'No tiles specified, terminating.'
  process.exit -1

if config.pid
  try
    pid = npid.create config.pid
    pid.removeOnExit()
  catch error
    logger.log 'error', 'Can not manage PID file %s, terminating.', config.pid
    process.exit -1

# Set up configuration

defaultImage = if config.default_tile
  try
    fs.readFileSync config.default_tile
  catch error
    logger.log 'warn', 'Can not read default tile image.
                Message: ' + error.message
    undefined

databases = config.tiles.map (path) -> new sqlite3.Database path

# Server

app = express()
router = express.Router()

app.all '*', (request, response, next) ->
  return next() if !request.get 'Origin'

  url = request.url
  remoteAddress = request.connection.remoteAddress

  logger.log 'info', '%s GET %s', remoteAddress, url

  response.set
    'Access-Control-Allow-Origin':  '*'
    'Access-Control-Allow-Methods': 'GET'
    'Access-Control-Allow-Headers': 'X-Requested-With'

  return response.send 200 if 'OPTIONS' == request.method
  next()

app.use router

getTileJSON = (database, callback) ->
  port = !config.socket && config.port
  host = 'http://' + config.host
  host = host + ':' + port if port

  metadata =
    tiles: []
    scheme: 'tms'
    tilejson: '2.0.0'

  database.all 'SELECT * FROM metadata', (error, results) ->
    logger.log 'debug', error if error
    if results
      json = _(results).chain().filter (result) -> result.name == 'json'
      .first()
      .value()

      _(results).chain().filter (result) -> result.name != 'json'
      .each (result) -> metadata[result.name] = result.value

      metadata.center = metadata.center.split ',' if metadata.center
      metadata.bounds = metadata.bounds.split ',' if metadata.bounds

      metadata.format = 'png' unless metadata.format
      metadata.tiles.push host + '/{z}/{x}/{y}.' + metadata.format
      metadata = _(metadata).extend JSON.parse json.value if json?.value?

      app.set 'format', metadata.format
      callback metadata

getTile = (databases, tileParams, callback, i=0) ->
  if i >= databases.length
    callback()
    return false

  {0: z, 1: x, 2: y} = tileParams
  y = Math.pow(2, z) - y - 1 if config.force_tms

  databases[i].get "SELECT tile_data FROM tiles
                    WHERE zoom_level = #{z}
                    AND tile_column = #{x}
                    AND tile_row = #{y}", (error, results) ->
    logger.log 'debug', error if error
    if results
      callback results
    else
      getTile databases, tileParams, callback, i+1

router.get '/source.tilejson', (request, response) ->
  getTileJSON _(databases).first(), (metadata) ->
    response.set
      'Content-Type': 'application/json'
    
    response.send JSON.stringify metadata

router.get /^\/(\d+)\/(\d+)\/(\d+)(?:\.[a-zA-Z]{1,10}){0,1}$/, (request, response) ->
  url = request.url
  remoteAddress = request.connection.remoteAddress

  getTile databases, request.params, (tile) ->
    if tile
      if app.get('format') == 'pbf'
        response.set
          'Content-Type':     'application/x-protobuf'
          'Content-Encoding': 'gzip'

      response.send tile.tile_data
    else
      logger.log 'warn', '%s GET %s not found', remoteAddress, url
      
      status = if defaultImage then 206 else 404
      response.status(status).send defaultImage || 'Not found'

database = _(databases).first()
database.get 'SELECT value FROM metadata WHERE name="format"', (error, results) ->
  logger.log 'debug', error if error
  if results
    format = results.value
    app.set 'format', format

  server = app.listen config.socket || config.port, () ->
    fs.chmodSync config.socket, '1766' if config.socket
    logger.log 'info', 'Server started.'

  # Catch SIGINT (Ctrl+C) to exit process
  process.on 'SIGINT', () ->
    logger.log 'warn', 'Caught SIGINT, terminating.'
    server.close()
    _(databases).each (db) -> db.close()
    process.exit()