# Getting Started

### Install using npm

```shell
npm install gerardus
```

### Building from source

```shell
git clone git@bitbucket.org:mystand/gerardus.git
cd gerardus
npm install
npm run build
```

### Usage

```shell
usage: gerardus [-h] [-v] [-c CONFIG] [-d DEFAULT_TILE] [--host HOST] [-l LOG]
                [-p PORT] [--pid PID] [-s SOCKET] [--verbose]
                [tiles [tiles ...]]

simplest mbtiles server

Positional arguments:
  tiles                 list of .mbtiles

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program version number and exit.
  -c CONFIG, --config CONFIG
                        Load the configuration found in filename.
  -d DEFAULT_TILE, --default-tile DEFAULT_TILE
                        Image that will get the client if the requested tile 
                        is not found
  --force-tms           Force response as TMS (default false)
  --host HOST           Server hostname
  -l LOG, --log LOG     Log file (default gerardus.log)
  -p PORT, --port PORT  Port (default 8888)
  --pid PID             PID file
  -s SOCKET, --socket SOCKET
                        Unix socket to listen
  --verbose             Verbose output (default false)
```

### Example 1

```shell
gerardus --verbose russia.mbtiles world.mbtiles
```

### Example 2

`config.yml`:

```yaml
default_tile: "default.png"
pid: "/var/tmp/gerardus.pid"
socket: "/var/tmp/http.sock"
tiles: ["russia.mbtiles", "world.mbtiles"]
```

```shell
gerardus --config config.yml
```

__Note__: passing additional arguments to application will rewrite configuration settings.

### Help

```shell
gerardus -h
```